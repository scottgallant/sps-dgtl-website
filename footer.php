<?php
/**
 * Theme footer.
 */
?>

<?php if ( !is_page( 116 ) && !is_front_page() ) : ?>
    </main>
<?php endif; ?>
<?php if ( !is_page_template( 'page_bio.php' ) ) : ?>
<footer id="site-footer">
	<section id="footer-sitemap">
        <a href="https://spsdgtl.com/#main"><img src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg"></a>
		<div>
			<div>
				<h6>Services</h6>
				<a href="/creative-services">Creative Services</a>
				<a href="/creative-content-distribution">Creative Content Distribution</a>
				<a href="/social-engagement-customer-care">Social Engagement &amp; Customer Care</a>
				<a href="/social-advertising">Social Advertising</a>
			</div>
			<div>
				<h6><a href="/learn">Learn</a></h6>
				<a href="https://blog.spsdgtl.com/">Blog</a>
				<a href="/learn#events">Events</a>
				<a href="/learn#webinars">Webinars</a>
				<a href="/learn#newsletter-signup">Newsletter</a>
			</div>
			<div>
				<h6><a href="https://spsdgtl.com/#main">SPS DGTL</a></h6><?php
                /* <a href="/work">Work</a> */ ?>
                <a href="/about">About</a>
                <a href="/about#team">Team</a>
				<a href="/about#opportunities">Opportunities</a>
                <div class="social-links">
                    <a href="https://www.linkedin.com/company/spsdgtl/" class="fa fa-linkedin"></a>
                    <a href="https://www.facebook.com/spsdgtl/" class="fa fa-facebook-official"></a>
                    <a href="https://twitter.com/spsdgtl" class="fa fa-twitter"></a>
                    <a href="https://www.instagram.com/spsdgtl/" class="fa fa-instagram"></a>
                </div>
				<a href="/contact" id="footer-contact-link" class="cta outline-light alt">Contact</a>
			</div>
		</div>
	</section>
    <section id="footer-copyright">
        &copy; 2015 - <?php echo date('Y'); ?> SPS DGTL, a division of <a href="http://ylconsulting.com" >Y&amp;L Consulting Inc.</a>
    </section>
</footer>
<?php endif; ?>
<?php if ( is_page( 116 ) || is_front_page() ) : ?>
    </main>
</div> <!-- #site-container -->
<?php endif; ?>
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:722177,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<?php wp_footer(); ?>

</body>
</html>