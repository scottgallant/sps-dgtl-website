<?php
/*
 * Single Page Template
 */

get_header();
?>
    <article>
        <h2 id="page-title"><?php the_title(); ?></h2>
        <?php the_content(); ?>
    </article>
<?php
get_sidebar( 'newsletter' );
get_footer();