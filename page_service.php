<?php
/*
 * Template Name: Service
 */

get_header();

$results = get_field( 'results_data' );
?>
    <article>
        <h2 id="page-title"><?php the_title(); ?></h2>
        <?php the_content(); ?>
        <aside id="services-detail">
            <h3>Services</h3>
            <div>
			    <?php echo str_replace( '<p>&nbsp;</p>', '', get_field( 'services_list' ) ); ?>
            </div>
        </aside>
        <?php if ( !is_page( 287 ) ) : ?>
            <h3 class="work-title">
                <span id="case-study-tag">Case Study</span>
                <b><?php the_field('case_study_client'); ?></b>
                <i><?php the_field('case_study_statement'); ?></i>
            </h3>
        <div class="work-sections">
            <section class="approach">
                <div>
                    <p><?php the_field('box_1'); ?></p>
                    <p><?php the_field('box_2'); ?></p>
                    <p><?php the_field('box_3'); ?></p>
                </div>
	            <?php the_field('approach_details'); ?>
            </section>
            <section class="results">
                <div class="chart-labels">
<?php
    $results_which = get_field( 'results_data' );
    if ($results_which === '1') : ?>
                    <h4 class="chart-label total">
                        <span class="text">Click-Through Rate Increase</span>
                        <span class="percent">1236%</span>
                    </h4>
                    <h4 class="chart-label total alt">
                        <span class="text">Cost-Per-Click Reduction</span>
                        <span class="percent">44%</span>
                    </h4>
                    <h4 class="chart-label over-time">Clicks &amp; Cost Over Time</h4>
<?php else: ?>
                    <h4 class="chart-label total">
                        <span class="text">Total Audience Growth</span>
                        <span class="value">84,128</span>
                        <span class="percent">4.9%</span>
                    </h4>
                    <h4 class="chart-label growth">
                        <span class="text">Audience Growth by Social Channel</span>
                        <section class="networks">
                            <section class="facebook">
                                <span class="value">77,889</span>
                                <span class="percent">5.7%</span>
                                <span class="text">Facebook</span>
                            </section>
                            <section class="instagram">
                                <span class="value">1,461</span>
                                <span class="percent">2.2%</span>
                                <span class="text">Instagram</span>
                            </section>
                            <section class="twitter">
                                <span class="value">4,668</span>
                                <span class="percent">2.2%</span>
                                <span class="text">Twitter</span>
                            </section>
                        </section>
                    </h4>
                    <h4 class="chart-label over-time">Audience Growth Over Time</h4>
<?php endif; ?>
                </div>
                <div id="audience-growth-chart-container">
                    <canvas id="audience-growth-chart" class="chart"></canvas>
                </div>
                <img id="audience-growth-chart-image" src="<?php the_field( 'chart_image' ); ?>">
            </section>
            <button class="work-sections-toggle cta alt">
                <span>The Results</span>
                <span>The Approach</span>
            </button>
        </div><?php
        endif; ?>
	    <?php the_field( 'final_content' ); ?>
    </article>
    <nav class="services-nav bottom"><?php
        $prev_id = get_field( 'prev_page', false, false );
	    $next_id = get_field( 'next_page', false, false ); ?>
        <a href="<?php echo get_the_permalink($prev_id); ?>"><?php echo get_the_title($prev_id); ?></a>
        <a href="<?php echo get_the_permalink($next_id); ?>"><?php echo get_the_title($next_id); ?></a>
    </nav>
<?php
$scripts = array();

// Major Multinational Retailer
$scripts['0'] = <<<'SCRIPT'
        ctx = document.getElementById("audience-growth-chart").getContext("2d");

        Chart.defaults.global.defaultFontFamily = 'filson-pro, sans-serif';
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = 'white';

        new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Total Audience (Followers, Fans, Friends)',
                        data: [1649606, 1654314, 1656993, 1662182, 1678936, 1695530, 1708365, 1711768, 1712673, 1713611, 1714223, 1724586, 1732506, 1733162],
                        yAxisID: 'total',
                        type: 'line',
                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    },
                    {
                        label: 'Facebook',
                        data: [485, 5021, 2458, 4546, 16205, 15737, 13290, 2248, 662, 556, 591, 10767, 7933, 614],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(145, 168, 210, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Instagram',
                        data: [21, 13, 101, 125, 147, 140, 134, 244, 49, 274, 163, 11, 93, 114],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(175, 191, 222, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Twitter',
                        data: [73, 359, 454, 168, 852, 452, 334, 106, 51, 183, 197, 114, 135, 222],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(202, 219, 255, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }
                ],
                labels: ['Aug 24', 'Aug 25', 'Aug 26', 'Aug 27', 'Aug 28', 'Aug 29', 'Aug 30', 'Aug 31', 'Sept 1', 'Sept 2', 'Sept 3', 'Sept 4', 'Sept 5', 'Sept 6']
            },
            options: {
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        id: 'growth',
                        type: 'linear',
                        position: 'right',
                        stacked: true,
                        ticks: {
                            stepSize: 5000,
                            max: 25000
                        }
                    }, {
                        id: 'total',
                        type: 'linear',
                        position: 'left',
                        stacked: true,
                        ticks: {
                            stepSize: 25000,
                            min: 1625000,
                            max: 1750000
                        }
                    }
                    ]
                }
            }
        });

        var sections = document.querySelector('.work-sections');
        document.querySelector('.work-sections-toggle').addEventListener('click', function () {
            sections.classList.toggle('second-section');
        });
SCRIPT;

// Rare Coin Collectibles
$scripts['1'] = <<<'SCRIPT'
        ctx = document.getElementById("audience-growth-chart").getContext("2d");

        Chart.defaults.global.defaultFontFamily = 'filson-pro, sans-serif';
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = 'white';

        new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [
                    {
                        label: 'Cost per Click',
                        data: [0.49, 0.73, 0.59, 0.99, 0.96, 0.08, 0.16, 0.09, 0.09, 0.09, 0.11, 0.10, 0.11, 0.10, 0.10, 0.10, 0.10, 0.09, 0.09, 0.07, 0.08, 0.05, 0.05],
                        yAxisID: 'cost',
                        backgroundColor: 'rgba(175, 191, 222, 0.2)',
                        borderColor: 'rgba(175, 191, 222, 1.0)'
                    },
                    {
                        label: 'Link Clicks',
                        data: [34.40, 26.40, 29.10, 21.20, 37.67, 388.80, 189.57, 360.14, 339.14, 322.14, 281.14, 295.86, 292.86, 312.57, 317.71, 212.22, 157.27, 181.33, 279.78, 440.67, 379.00, 573.33, 602.00],
                        yAxisID: 'clicks',
                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }
                ],
                labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7', 'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12', 'Week 13', 'Week 14', 'Week 15', 'Week 16', 'Week 17', 'Week 18', 'Week 19', 'Week 20', 'Week 21', 'Week 22', 'Week 23']
            },
            options: {
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        id: 'cost',
                        type: 'linear',
                        position: 'left',
                        stacked: true
                    }, {
                        id: 'clicks',
                        type: 'linear',
                        position: 'right',
                        stacked: true,
                        ticks: {
                            max: 1000
                        }
                    }]
                }
            }
        });

        var sections = document.querySelector('.work-sections');
        document.querySelector('.work-sections-toggle').addEventListener('click', function () {
            sections.classList.toggle('second-section');
        });
SCRIPT;

// High-End Living Structures
$scripts['2'] = <<<'SCRIPT'
        ctx = document.getElementById("audience-growth-chart").getContext("2d");

        Chart.defaults.global.defaultFontFamily = 'filson-pro, sans-serif';
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = 'white';

        new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Total Audience (Followers, Fans, Friends)',
                        data: [1649606, 1654314, 1656993, 1662182, 1678936, 1695530, 1708365, 1711768, 1712673, 1713611, 1714223, 1724586, 1732506, 1733162],
                        yAxisID: 'total',
                        type: 'line',
                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    },
                    {
                        label: 'Facebook',
                        data: [485, 5021, 2458, 4546, 16205, 15737, 13290, 2248, 662, 556, 591, 10767, 7933, 614],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(145, 168, 210, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Instagram',
                        data: [21, 13, 101, 125, 147, 140, 134, 244, 49, 274, 163, 11, 93, 114],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(175, 191, 222, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Twitter',
                        data: [73, 359, 454, 168, 852, 452, 334, 106, 51, 183, 197, 114, 135, 222],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(202, 219, 255, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }
                ],
                labels: ['Aug 24', 'Aug 25', 'Aug 26', 'Aug 27', 'Aug 28', 'Aug 29', 'Aug 30', 'Aug 31', 'Sept 1', 'Sept 2', 'Sept 3', 'Sept 4', 'Sept 5', 'Sept 6']
            },
            options: {
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        id: 'growth',
                        type: 'linear',
                        position: 'right',
                        stacked: true,
                        ticks: {
                            stepSize: 5000,
                            max: 25000
                        }
                    }, {
                        id: 'total',
                        type: 'linear',
                        position: 'left',
                        stacked: true,
                        ticks: {
                            stepSize: 25000,
                            min: 1625000,
                            max: 1750000
                        }
                    }
                    ]
                }
            }
        });

        var sections = document.querySelector('.work-sections');
        document.querySelector('.work-sections-toggle').addEventListener('click', function () {
            sections.classList.toggle('second-section');
        });
SCRIPT;

wp_enqueue_script( 'spsdgtl-chartjs', get_stylesheet_directory_uri() . '/js/chart.js', array(), '2.7.1', true );
wp_add_inline_script('spsdgtl-chartjs', $scripts[ $results_which ] );

get_footer();