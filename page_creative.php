<?php
/*
 * Template Name: Creative Services
 */

get_header();
?>
    <article>
        <h2 id="page-title"><?php the_title(); ?></h2>
		<?php the_content(); ?>
        <section id="portfolio">
            <div id="controls">
                <h3>Portfolio</h3>
                <select id="portfolio-select" title="Filter Portfolio">
                    <option value="branded">Branded Content</option>
                    <option value="photography">Photography</option>
                    <option value="design">Design</option>
                </select>
                <label for="portfolio-select" class="fa fa-caret-down"></label>
            </div>
            <div id="portfolio-container" class="container branded">
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/dc03.png"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/tc02.jpg"></div>
                </figure>
                <figure class="card photography">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/ito01.jpg"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img class="portrait" src="/wp-content/uploads/2017/12/cw01.png"></div>
                </figure>
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/rb02.png"></div>
                </figure>
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/tca01.png"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/tc01.jpg"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/svo01.jpg"></div>
                </figure>
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/yl01.png"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/dc01.png"></div>
                </figure>
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/rb01.png"></div>
                </figure>
                <figure class="card photography">
                    <div class="back"></div>
                    <div class="front"><img class="portrait" src="/wp-content/uploads/2017/12/ito02.jpg"></div>
                </figure>
                <figure class="card branded">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/ss01.png"></div>
                </figure>
                <figure class="card photography">
                    <div class="back"></div>
                    <div class="front"><img class="portrait" src="/wp-content/uploads/2017/12/ito03.jpg"></div>
                </figure>
                <figure class="card design">
                    <div class="back"></div>
                    <div class="front"><img src="/wp-content/uploads/2017/12/sd01.gif"></div>
                </figure>
                <div id="lightbox">
                    <button id="lightbox-close" class="fa fa-close"></button>
                </div>
            </div>
        </section>
        <section class="creative-cta">
			<?php the_field( 'final_content' ); ?>
        </section>
    </article>
    <nav class="services-nav bottom"><?php
		$prev_id = get_field( 'prev_page', false, false );
		$next_id = get_field( 'next_page', false, false ); ?>
        <a href="<?php echo get_the_permalink($prev_id); ?>"><?php echo get_the_title($prev_id); ?></a>
        <a href="<?php echo get_the_permalink($next_id); ?>"><?php echo get_the_title($next_id); ?></a>
    </nav>
<?php
$script = <<<'SCRIPT'
    portfolio = document.getElementById('portfolio');
    portfolioSelect = document.getElementById('portfolio-select');
    portfolioContainer = document.getElementById('portfolio-container');

    portfolioSelect.addEventListener('change', function () {
        portfolioContainer.className = "container " + portfolioSelect.value;
    });

    new Waypoint({
        element: portfolio,
        handler: function(direction) {
            document.body.classList.toggle( 'attached-controls' , direction === 'down' );
        }
    });

    portfolioData = [
    
        {
            category: "branded",
            videoUrl: "/wp-content/uploads/2018/01/01-dc.mp4"
        },
    
        {
            category: "design"
        },
    
        {
            category: "photography"
        },
    
        {
            category: "design",
            isPortrait: true
        },
    
        {
            category: "branded"
        },
    
        {
            category: "branded"
        },
    
        {
            category: "design"
        },
    
        {
            category: "design"
        },
    
        {
            category: "branded"
        },
    
        {
            category: "design"
        },
    
        {
            category: "branded",
            videoUrl: "/wp-content/uploads/2018/01/11-rb.mov"
        },
    
        {
            category: "photography",
            isPortrait: true
        },
    
        {
            category: "branded"
        },
    
        {
            category: "photography",
            isPortrait: true
        },
    
        {
            category: "design"
        },
    
    ];

    lightbox = document.getElementById( 'lightbox' );
    lightboxClose = document.getElementById( 'lightbox-close' );
    lightboxClose.addEventListener( 'click' , function () {
        document.body.classList.remove( 'lightbox-active' );
        document.querySelectorAll( '#lightbox video' ).forEach( function ( element ) {
            element.pause();
        });
    });

    var categoryIndices = {
        "branded": 0,
        "photography": 0,
        "design": 0
    };
    document.querySelectorAll( '#portfolio-container .card .front img' ).forEach( function ( element, index ) {
        var item = portfolioData[ index ];
        var categoryIndex = categoryIndices[ item.category ];
        categoryIndices[ item.category ]++;

        var lightboxFigure = document.createElement( 'figure' );
        lightboxFigure.className = item.category;

        var lightboxItem;
        if ( item.videoUrl ) {
            lightboxItem = document.createElement( 'video' );
            lightboxItem.src = item.videoUrl;
            lightboxItem.preload = 'metadata';
            lightboxItem.controls = true;
        }
        else {
            lightboxItem = document.createElement( 'img' );
            lightboxItem.src = element.src;
        }

        lightboxFigure.appendChild(lightboxItem);
        lightbox.appendChild(lightboxFigure);

        element.addEventListener( 'click' , function () {
            document.body.classList.add( 'lightbox-active' );
            lightbox.dataset.index = categoryIndex.toString();
        });
    });
SCRIPT;

wp_add_inline_script('spsdgtl-main', $script, 'after');
get_footer();