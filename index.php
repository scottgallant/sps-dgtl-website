<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>SPS DGTL - Coming Soon</title>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="/wp-content/uploads/2017/09/sps-dgtl-logo-a-minimal.svg" rel="icon">

    <script src="https://use.typekit.net/nrr2csk.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body>
<header id="site-header" class="no-parallax">
    <h2 id="coming-soon">Coming Soon</h2>
    <object type="image/svg+xml" data="/wp-content/uploads/2017/09/sps-dgtl-logo-transparent-animated.svg">SPS DGTL</object>
    <a id="current-website" href="http://socialpathsolutions.com/">Current Website</a>
</header>
</body>
</html>