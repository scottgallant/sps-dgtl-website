<?php
/**
 * The newsletter sidebar.
 */
if ( ! is_active_sidebar( 'newsletter' ) ) {
	return;
}
?>

<aside id="newsletter-signup" class="widget-area">
	<h2>Receive our newsletter!</h2>
	<?php dynamic_sidebar( 'newsletter' ); ?>
</aside><!-- #secondary -->