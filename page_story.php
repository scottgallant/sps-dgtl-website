<?php
/*
 * Template Name: Story
 */

get_header();
?>
    <article id="story">
        <h3 id="page-title">Our Story</h3>
        <div id="the-ori">
            <?php the_content(); ?>
        </div>
        <?php the_field( 'textarea' ); ?>
        <h3 id="opportunities">Opportunities</h3>
        <p style="text-align: center">Coming soon!</p>
    </article>
<?php
get_sidebar( 'newsletter' );
get_footer();