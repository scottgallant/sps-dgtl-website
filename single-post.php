<?php
/*
 * Single Post Template
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <header id="post-header">
        <section id="post-banner"><?php
	        if ( get_the_ID() === 426 ): ?>
                <img id="featured-image" src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg"><?php
	        else: ?>
                <img id="featured-image" src="<?php the_post_thumbnail_url( 'full' ); ?>"><?php
	        endif; ?>
            <h3 id="categories"><?php
foreach( ( get_the_category() ) as $category ) :

?><a class="category-link" href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name . ' '; ?></a><?php

endforeach;

?></h3>
            <h2 id="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </section>
        <section id="post-meta"><?php $author_id = get_the_author_meta( 'ID' ); ?>
            <a id="author-avatar" href="<?php get_author_posts_url( $author_id ); ?>"><img src="<?php echo get_avatar_url( $author_id , array( 'size' => 512 ) ); ?>"></a>
            <a id="author" href="<?php get_author_posts_url( $author_id ); ?>"><?php the_author(); ?></a>
	        <time datetime="<?php echo get_the_date('Y-m-d\TH:i'); ?>">written on <?php echo get_the_date('M d, Y'); ?></time>
        </section>
    </header>
    <article id="post">
	    <?php the_content(); ?>
    </article>
<?php endwhile; endif; ?>
    <aside id="blog-sidebar">

    </aside>
<?php
get_footer();