<?php
/*
 * Template Name: Bio
 */

get_header();
?>
    <figure class="photos">
        <img src="<?php the_field('photo'); ?>" class="photo">
    </figure>
    <div class="info">
        <h1 class="name"><?php the_field('fn'); ?> <?php the_field('ln'); ?></h1>
        <h2 class="title"><?php the_field('job_title'); ?></h2>
        <p class="description"><?php the_field('biography'); ?></p>
        <div class="social">
			<?php if ( !empty( get_field('social_url_1') ) && !empty( get_field('social_1') ) ) : ?>
                <a href="<?php the_field('social_url_1'); ?>" class="fa fa-<?php the_field('social_1'); ?>"></a>
			<?php endif; ?>
			<?php if ( !empty( get_field('social_url_2') ) && !empty( get_field('social_2') ) ) : ?>
                <a href="<?php the_field('social_url_2'); ?>" class="fa fa-<?php the_field('social_2'); ?>"></a>
			<?php endif; ?>
			<?php if ( !empty( get_field('social_url_3') ) && !empty( get_field('social_3') ) ) : ?>
                <a href="<?php the_field('social_url_3'); ?>" class="fa fa-<?php the_field('social_3'); ?>"></a>
			<?php endif; ?>
        </div>
    </div>
<?php
function json_ld_script() {
?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Person",
        "image": "'<?php the_field('photo'); ?>",
        "name": "<?php the_field('fn'); ?> <?php the_field('ln'); ?>",
        "jobTitle": "<?php the_field('job_title'); ?>",
        "url": "<?php echo get_permalink(); ?>",
        "sameAs" : [
<?php if ( !empty( get_field('social_url_1') ) && !empty( get_field('social_1') ) ) : ?>
            "<?php the_field('social_url_1'); ?>"<?php endif; ?><?php if ( !empty( get_field('social_url_2') ) && !empty( get_field('social_2') ) ) : ?>,
            "<?php the_field('social_url_2'); ?>"<?php endif; ?><?php if ( !empty( get_field('social_url_3') ) && !empty( get_field('social_3') ) ) : ?>,
            "<?php the_field('social_url_3'); ?>"
<?php endif; ?>

        ]
    }
</script>
<?php
}
add_action( 'wp_footer', 'json_ld_script' );

get_footer();