<?php
/*
 * Template Name: Technically Home
 */

get_header();
?>
    <a id="skip-to-main" href="#main"><span></span><span></span><span></span></a>
    <article id="mission-statement">
        <h2><?php the_field('mission_statement'); ?></h2>
    </article>
    <article id="locations">
        <figure class="illustration">
            <img id="locations-illustration" src="/wp-content/uploads/2017/09/sps-dgtl-cities.svg">
            <figcaption id="locations-titles">
                <div id="location-los-angeles" class="location"><?php the_field('location_los_angeles'); ?></div>
                <div id="location-san-antonio" class="location"><?php the_field('location_san_antonio'); ?></div>
                <div id="location-tampa" class="location"><?php the_field('location_tampa'); ?></div>
            </figcaption>
        </figure>
        <h2><?php the_field('locations_info'); ?></h2>
    </article>
    <article id="our-story">
        <div class="container">
            <h2><?php the_field('our_story_title'); ?></h2>
			<?php the_field('our_story_content'); ?>
            <p>
                <a href="/story" class="cta outline-light arrow"><?php the_field('our_story_cta'); ?></a>
            </p>
        </div>
    </article>
    <article id="connect">
        <h2><?php the_field('connect_title'); ?></h2>
		<?php the_field('connect_content'); ?>
        <div class="cards">
            <a href="/creative-services">Creative Services<img src="/wp-content/uploads/2017/12/easel.png"></a>
            <a href="/creative-content-distribution">Creative Content Distribution<img src="/wp-content/uploads/2017/12/typewriter.png"></a>
            <a href="/social-advertising">Social Advertising<img src="/wp-content/uploads/2017/12/billboard.png"></a>
        </div>
    </article>
    <article id="engage">
        <h2><?php the_field('engage_title'); ?></h2>
		<?php the_field('engage_content'); ?>
        <div class="cards">
            <a href="/social-engagement-customer-care">Social Engagement &amp; Customer Care<img src="/wp-content/uploads/2017/12/phone.png"></a>
        </div>
    </article>
    <article id="grow">
        <h2><?php the_field('grow_title'); ?></h2>
		<?php the_field('grow_content'); ?>
    </article>
    <article id="case-study">
        <h3 class="work-title">
            <span id="case-study-tag">Case Study</span>
            <b><?php the_field('case_study_client'); ?></b>
            <i><?php the_field('case_study_statement'); ?></i>
        </h3>
        <div class="work-sections">
            <section class="approach">
                <div>
                    <p><?php the_field('box_1'); ?></p>
                    <p><?php the_field('box_2'); ?></p>
                    <p><?php the_field('box_3'); ?></p>
                </div>
		        <?php the_field('approach_details'); ?>
            </section>
            <section class="results">
                <div class="chart-labels">
                    <h4 class="chart-label total">
                        <span class="text">Total Audience Growth</span>
                        <span class="value">1080</span>
                        <span class="percent">110.3%</span>
                    </h4>
                    <h4 class="chart-label growth">
                        <span class="text">Audience Growth by Social Channel</span>
                        <section class="networks">
                            <section class="facebook">
                                <span class="value">1,238</span>
                                <span class="percent"> </span>
                                <span class="text">Facebook</span>
                            </section>
                            <section class="instagram">
                                <span class="value">49</span>
                                <span class="percent"> </span>
                                <span class="text">Instagram</span>
                            </section>
                            <section class="twitter">
                                <span class="value">87</span>
                                <span class="percent"> </span>
                                <span class="text">Twitter</span>
                            </section>
                        </section>
                    </h4>
                    <h4 class="chart-label over-time">Audience Growth Over Time</h4>
                </div>
                <div id="audience-growth-chart-container">
                    <canvas id="audience-growth-chart" class="chart"></canvas>
                </div>
                <img id="audience-growth-chart-image" src="<?php the_field( 'chart_image' ); ?>">
            </section>
            <button class="work-sections-toggle cta alt">
                <span>The Results</span>
                <span>The Approach</span>
            </button>
        </div>
    </article>
<?php get_sidebar( 'newsletter' ); ?>
    <article id="blog">
        <h2><a href="https://blog.spsdgtl.com/"><?php _e('Blog', 'spsdgtl'); ?></a></h2>
        <section id="blog-posts">
            <a class="blog-post" href="https://blog.spsdgtl.com/new-webinar-series" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/camera_featured.jpg?t=1516300458116">
                <h4 class="title">3 Reasons Why You Can’t Miss Our New Webinar Series</h4>
            </a>
            <a class="blog-post" href="https://blog.spsdgtl.com/facebook-news-feed-update" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/three-handed-monster-resized.png?t=1516300458116">
                <h4 class="title">The Facebook News Feed Update: What to Focus on Now</h4>
            </a>
            <a class="blog-post" href="https://blog.spsdgtl.com/digital-communications-in-mexico" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/mexicoplane-027231-edited.jpg?t=1516300458116">
                <h4 class="title">Why WhatsApp Could Be the Answer to Digital Communications in Mexico</h4>
            </a>
        </section>
        <a href="https://blog.spsdgtl.com/" class="cta arrow alt">View More</a>
    </article>
<?php
$script = <<<'SCRIPT'
        ctx = document.getElementById("audience-growth-chart").getContext("2d");

        Chart.defaults.global.defaultFontFamily = 'filson-pro, sans-serif';
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = 'white';

        new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Total Audience (Followers, Fans, Friends)',
                        data: [979, 1081, 1301, 1348, 1411, 1527, 1675, 1812, 1906, 1984, 2034, 2059],
                        yAxisID: 'total',
                        type: 'line',
                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    },
                    {
                        label: 'Facebook',
                        data: [120, 101, 225, 46, 66, 125, 147, 141, 105, 78, 51, 33],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(145, 168, 210, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Instagram',
                        data: [5, 5, 5, 2, 5, 8, 3, 2, 4, 2, 4, 4],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(175, 191, 222, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Twitter',
                        data: [1, 7, 4, 4, 11, 2, 16, 12, 5, 5, 14, 6],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(202, 219, 255, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }
                ],
                labels: ['Aug', 'Sept', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul']
            },
            options: {
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        id: 'growth',
                        type: 'linear',
                        position: 'right',
                        stacked: true
                    }, {
                        id: 'total',
                        type: 'linear',
                        position: 'left',
                        stacked: true
                    }
                    ]
                }
            }
        });

        var sections = document.querySelector('.work-sections');
        document.querySelector('.work-sections-toggle').addEventListener('click', function () {
            sections.classList.toggle('second-section');
        });
SCRIPT;

wp_enqueue_script( 'spsdgtl-chartjs', get_stylesheet_directory_uri() . '/js/chart.js', array(), '2.7.1', true );
wp_add_inline_script('spsdgtl-chartjs', $script);

get_footer();